# Gitflow HOWTO:

## Preparing project for the first time:

### Download and intall `gitflow` according to your Operating System:

[`gitflow-avh`](https://github.com/petervanderdoes/gitflow-avh/wiki/Installation)

### Create new project and initialize git:

    `mkdir project_name && cd project_name`

    `git init .`

### Initialize git-flow with default naming conventions (-d)

    `git flow init -d`

### Do "New repo" in github and add it to local project:

    `git remote add origin git@github.com:username/repo-name.git`

### Push master to remote repo:

    `git checkout master`

    `git push origin HEAD`

### Push develop to remote repo:

    `git checkout develop`

    `git push origin HEAD`

### ! Change default branch to develop in github.com

      Go to github.com -> 'master' -> 'view all branches' -> 'change default branch' -> 'develop' -> push 'Update' button
      Go to gitlab.com -> current_repository ->  settings -> repository -> branch defaults -> default branch -> develop -> save changes

## Normal work with GIT-FLOW after preparing

### Start new feature:

    `git flow feature start my-feature`

    `git branch`

### Add some new edits to feature:

    `echo 'Add new feature' > README.md`

    `git add .`

    `git commit -m 'Create README.md'`

### Push feature/my-feature to the develop branch:

    `git flow feature publish my-feature`

### Open pull-request in github.com/repo_name

    "Compare & pull request" or "Create merge request"

    Title: Feature |task_number_in_JIRA| Create readme.md
    Text: Create readme.md

    "Create pull/merge request"

### Add label "work in progress"

### If need review add label "need review"

### If two approves -> Merge to develop

    "Merge pull request"
    "Confirm merge"

### After merge delete branch:

    "Delete branch"

### Sync develop branch

    `git checkout develop`

    `git pull origin develop`

### Remove merged feature branch locally

    `git flow feature delete my-feature`

## Release

### Creating release from develop branch:

    git flow release start 0.1.0

### Commit

    echo 'Release 0.1.0' > CHANGELOG.md
    git add .
    git commit -m 'Bump to 0.1.0'

### Finish

    git flow release finish 0.1.0

### Push

    git push --all // pushes both branches: master and develoop
    git push --tags

## Hotfix

### Creating hotfix

    git flow hotfix start 0.1.1

### Commit

    echo 'Update instructions' > README.md
    echo 'Release 0.1.1: - fixed localhost error bug' > CHANGELOG.md

    git add .

    git commit -m 'Change readme instructions'

### Finish

    git flow hotfix finish 0.1.1

### Push

    git push --all
    git push --tags

## Useful links:

https://rubygarage.org/blog/git-and-release-management-workflow

https://rubygarage.github.io/slides/git/gitflow#/
